{
    "id": "30461eba-6578-47e3-912a-be42c1b4e9cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8380fdf-3e0b-4b07-b14c-8aee2f39b818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30461eba-6578-47e3-912a-be42c1b4e9cc",
            "compositeImage": {
                "id": "8197dc92-1678-42eb-9248-5ad27f5c7d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8380fdf-3e0b-4b07-b14c-8aee2f39b818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "932c3826-4ba6-4a8f-a97f-0891088cd4fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8380fdf-3e0b-4b07-b14c-8aee2f39b818",
                    "LayerId": "15a68a71-7e26-4606-ab29-1d6ff99726b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "15a68a71-7e26-4606-ab29-1d6ff99726b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30461eba-6578-47e3-912a-be42c1b4e9cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}