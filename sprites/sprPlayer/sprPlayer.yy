{
    "id": "93c035ac-0a81-44bc-9a53-b6839a5a6d2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dd53c07-b507-4e0c-923f-b175236b117e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c035ac-0a81-44bc-9a53-b6839a5a6d2e",
            "compositeImage": {
                "id": "f7317059-7de4-4483-99b6-d93d0d36d9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dd53c07-b507-4e0c-923f-b175236b117e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b2bc3a-edee-4124-9de9-597f3215b485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dd53c07-b507-4e0c-923f-b175236b117e",
                    "LayerId": "2468e989-a365-4b19-ad0b-2174de751e8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2468e989-a365-4b19-ad0b-2174de751e8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93c035ac-0a81-44bc-9a53-b6839a5a6d2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}