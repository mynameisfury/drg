{
    "id": "c49f052a-8640-4bf7-ba03-9ac049944c0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPlayer",
    "eventList": [
        {
            "id": "4e4621d4-e087-459c-a64f-469f9d7e92a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c49f052a-8640-4bf7-ba03-9ac049944c0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "565b0d0a-988a-477b-bc83-e7e819f5221e",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "e9932a44-71e6-46b9-9fce-235fe3e7f224",
            "propertyId": "22de3844-796b-4c89-9ecc-8bf019af05f9",
            "value": "6"
        },
        {
            "id": "e84ce433-79f4-4de4-8053-f2304adba740",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "e9932a44-71e6-46b9-9fce-235fe3e7f224",
            "propertyId": "f6840db8-363d-48c1-9420-9ddbd08e1480",
            "value": "15"
        }
    ],
    "parentObjectId": "e9932a44-71e6-46b9-9fce-235fe3e7f224",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "12e9efe1-dc71-49b1-8c4e-3f95fde37273",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "abilityCooldown",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "93c035ac-0a81-44bc-9a53-b6839a5a6d2e",
    "visible": true
}