{
    "id": "9ce5c662-4dc3-49b1-b7db-7f041721fa66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPickup",
    "eventList": [
        {
            "id": "ee16c06f-8583-4094-a941-6a6e5ef726d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9ce5c662-4dc3-49b1-b7db-7f041721fa66"
        },
        {
            "id": "9f38cefe-fa00-444f-8b27-a040437588f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c49f052a-8640-4bf7-ba03-9ac049944c0e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9ce5c662-4dc3-49b1-b7db-7f041721fa66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}