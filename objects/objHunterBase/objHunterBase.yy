{
    "id": "3d43e8a0-0820-4f37-a671-51f802629224",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objHunterBase",
    "eventList": [
        {
            "id": "ac12762e-4570-4ea8-95f7-544f4185aae5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d43e8a0-0820-4f37-a671-51f802629224"
        },
        {
            "id": "9af5540b-ae09-43d3-ac6f-b27dfc05c7b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d43e8a0-0820-4f37-a671-51f802629224"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "c49f052a-8640-4bf7-ba03-9ac049944c0e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "da152034-5f64-432f-82ec-e4ec691fd80f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "baseDoubleJump",
            "varType": 1
        },
        {
            "id": "f4f58a2d-922c-45f6-8eeb-334d53b9d867",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "additionalJumps",
            "varType": 1
        },
        {
            "id": "5e122dac-d015-479b-8672-2b328c089d2b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "20",
            "varName": "dodgeTime",
            "varType": 0
        },
        {
            "id": "74c9f3a6-f0ba-4e0b-9634-86b2252d19fb",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "20",
            "varName": "dodgeSpeed",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "93c035ac-0a81-44bc-9a53-b6839a5a6d2e",
    "visible": true
}