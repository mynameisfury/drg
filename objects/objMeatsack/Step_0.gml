//movement
vsp = vsp + grv;

//if (vsp > 200){
//	vsp = 200
//}

//horizontal collision
if (place_meeting(x + hsp,y,objWall))
{
	while (!place_meeting(x+sign(hsp),y,objWall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}

var increment = sprite_width;
for(var i = hsp; i != 0; i = approach(i, 0, increment)){
    var spd = clamp(i, -increment, increment);
    if (place_meeting(x + spd,y,objWall)){
        while (!place_meeting(x+sign(hsp),y,objWall)){
            x = x + sign(hsp);
        }
        hsp = -hsp;
        break;
    }
    x += spd;
}


//x += hsp;

//vertical collision
if (place_meeting(x ,y + vsp,objWall))
{
	while (!place_meeting(x,y+sign(vsp),objWall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

var increment = sprite_height;
for(var i = vsp; i != 0; i = approach(i, 0, increment)){
    var spd = clamp(i, -increment, increment);
    if (place_meeting(x,y + spd,objWall)){
        while (!place_meeting(x,y+sign(vsp),objWall)){
            y = y + sign(vsp);
        }
        vsp = 0;
        break;
    }
    y += spd;
}


//y += vsp;

